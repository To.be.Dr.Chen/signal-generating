#include "logger.h"
#include <Arduino.h>

void Logger::initialize() {
  Serial.begin(38400);
}

void Logger::log_msg(const char* c, const char* s) {
  Serial.print("[");
  Serial.print(c);
  Serial.print("] ");
  Serial.println(s);
}

void Logger::log_msg(const char* c, const char* s, int i) {
  Serial.print("[");
  Serial.print(c);
  Serial.print("] ");
  Serial.print(s);
  Serial.print(" ");
  Serial.println(i);
}

void Logger::log_msg(const char* c, const char* s, double d) {
  Serial.print("[");
  Serial.print(c);
  Serial.print("] ");
  Serial.print(s);
  Serial.print(" ");
  Serial.println(d);
}

