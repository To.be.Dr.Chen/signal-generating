#ifndef _SUITE_H
#define _SUITE_H

namespace Suite {
  void initialize();
  void start();
  void stop();
  void pause();
  void next();
  void prev();
}

#endif
