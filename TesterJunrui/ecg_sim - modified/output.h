#ifndef _OUTPUT_H
#define _OUTPUT_H

namespace Output {

  void initialize();
  
  void display_test(int _test_no, int _duration, int _max_hr, int _min_hr, int _max_ptt, int _min_ptt, bool isRunning = true);
  void clear_display();

  void draw_display();
}

#endif
