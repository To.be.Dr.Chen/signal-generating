#ifndef _RUNNER_H
#define _RUNNER_H

class Test;

// External runner members
namespace Runner {
  enum AbortReason {
    FAILED = -1,
    NO_ABORT = 0,
    FINISH = 1,
    ABORT = 2,
    NEXT = 3,
    PREV = 4,
    PAUSE = 5
  };
  
  void initialize();
  AbortReason run(Test* test);
  void abort(AbortReason reason = ABORT);

  void set_ptt(unsigned int target_ptt_ms);
  void update_ptt();
  void inc_ptt();
  void dec_ptt();
  void set_heart_rate(unsigned int bpm);
  void update_heart_rate();
  void inc_heart_rate();
  void dec_heart_rate();
}

#endif
