#include "test.h"
#include "runner.h"
#include <DueTimer.h>

/*
 * Test1.
 * Herzfrequenz 30bpm; konstante Pulstransitzeit von 200ms; Dauer: 30 Sekunden
 */

Test1::Test1() : Test(30,30,30,200,200,0) {}

/*
 * Test2.
 * Herzfrequenz 200bpm; konstante Pulstransitzeit von 200ms; Dauer: 30 Sekunden
 */

Test2::Test2() : Test(30,200,200,200,200,0) {}

/*
 * Test3.
 * Herzfrequenz steigend von 30bpm auf 200bpm; konstante Pulstransitzeit von 200ms; Dauer: 60 Sekunden
 */

Test3::Test3() : Test(60,30,200,200,200,0) {}

/*
 * Test4.
 * Herzfrequenz fallend von 200bpm auf 30bpm; konstante Pulstransitzeit von 200ms; Dauer: 60 Sekunden
 */
Test4::Test4() : Test(60,200,30,200,200,0) {}

/*
 * Test5.
 * Konstante Herzfrequenz von 100bpm; konstante Pulstransitzeit von 100ms; Dauer: 30 Sekunden
 */
Test5::Test5() : Test(30,100,100,100,100,0) {}

/*
 * Test6.
 * Konstante Herzfrequenz von 100bpm; konstante Pulstransitzeit von 350ms; Dauer: 30 Sekunden
 */
Test6::Test6() : Test(30,100,100,350,350,0) {}

/*
 * Test7.
 * Konstante Herzfrequenz von 100bpm; Pulstransitzeit steigend von 100ms to 350ms; Dauer: 60 Sekunden
 */
Test7::Test7() : Test(60,100,100,100,350,0) {}

/*
 * Test8.
 * Konstante Herzfrequenz von 100bpm; Pulstransitzeit fallend von 350ms to 100ms; Dauer: 60 Sekunden
 */
Test8::Test8() : Test(60,100,100,350,100,0) {}

/*
 * Test9.
 * Konstante Herzfrequenz von 100bpm; Pulstransitzeit mit Sinus zwischen 200ms und 210ms; Dauer: 60 Sekunden
 */
Test9::Test9() : Test(60,100,100,200,210,0) {}

double sinus_ctr = 0.0;

// Gets called each 200ms
void sinus_ptt() {
  // After 20 calls (20*500ms=10s) the sine is completed.
  // This gives us 6 complete sinuses in the 60 seconds.
  sinus_ctr += (double)(2*PI)/20;
  double val = sin(sinus_ctr);
  Runner::set_ptt(205 + (int)round(5 * val));
}

void Test9::initialize() {
  Runner::set_heart_rate(100);
  Runner::set_ptt(205);
  sinus_ctr = 0.0;
  Timer6.stop();
  Timer6.attachInterrupt(sinus_ptt).setPeriod(500 * 1000);
  Timer6.start();
}

void Test9::finalize() {
  Timer6.stop();  
}

/*
 * Test10.
 * Herzfrequenz fallend von 30bpm auf 10bpm; konstante Pulstransitzeit von 200ms; Dauer: 30 Sekunden
 */
Test10::Test10() : Test(30,30,10,200,200,0) {}

/*
 * Test11.
 * Herzfrequenz steigend von 200bpm auf 250bpm; konstante Pulstransitzeit von 200ms; Dauer: 60 Sekunden
 */
Test11::Test11() : Test(60,200,250,200,200,0) {}

