#include "runner.h"
#include <DueTimer.h>
#include "pins.h"
#include "waveforms.h"
#include "test.h"
#include "logger.h"

using namespace Runner;

static Test* current_test = 0;
static unsigned int heart_rate = 60;
static unsigned int ptt_ms = 0;

static int sample = 0;
static int pleth_offset = 0; // Value between 0 and number of samples - 1

static AbortReason abort_reason = NO_ABORT;

static bool is_finished = true;

static bool do_heart_rate_update = true;
static bool do_ptt_update = true;

void sample_interrupt();
void finish();

void Runner::initialize() {
  // Set read/write resolution
  analogWriteResolution(12);
  analogReadResolution(12);

  pinMode(PIN_OUT_ECG, OUTPUT);
  pinMode(PIN_OUT_PLETH, OUTPUT);

  // Attach timer interrupts
  Timer4.attachInterrupt(sample_interrupt);
  Timer5.attachInterrupt(finish);
}

AbortReason Runner::run(Test* test) {
  if(!test) return FAILED;

  // Initialize the test
  current_test = test;
  current_test->initialize();
  Timer4.stop();
  Timer5.stop();
  Timer5.setPeriod(current_test->duration_sec * 1000 * 1000);

  // Run the test
  Logger::log_msg("RUNNER", "Running test...");
  abort_reason = NO_ABORT;
  is_finished = true;
  sample = 0;
  Timer5.start();
  while(abort_reason == NO_ABORT) {
    // Restart timers
    if(is_finished) {
      update_heart_rate();
      update_ptt();
      is_finished = false;
      Timer4.start();
    }
    delayMicroseconds(10);
  }
  // Shut down test
  Logger::log_msg("RUNNER", "Shutting down test with abort reason", abort_reason);
  Timer4.stop();
  Timer5.stop();
  analogWrite(PIN_OUT_PLETH, 0);
  analogWrite(PIN_OUT_ECG, 0);
  current_test->finalize();
  current_test = 0;
  return abort_reason;
}

void Runner::abort(AbortReason reason) {
  abort_reason = reason;
}

void finish() {
  abort(FINISH);
}

inline unsigned int mod(int a, int b) { return (unsigned int)(a%b+b)%b; }

void Runner::set_ptt(unsigned int target_ptt_ms) {
  if(!current_test) return;

  ptt_ms = target_ptt_ms;
  do_ptt_update = true;
}

void Runner::update_ptt() {
  if(!current_test) return;

  // Calculate the hold time for one sample with the currently used heart rate
  double wave_duration_us = (double)(60 * 1000 * 1000) / heart_rate;
  //Logger::log_msg("[RUNNER]", "Wave duration:", wave_duration_us);
  double sample_duration_us = (double)wave_duration_us / (double)waveTable[current_test->wave].no_samples;
  //Logger::log_msg("[RUNNER]", "Sample duration:", sample_duration_us);
  // Calculate the number of samples that the wave has to be set forward
  int total_offset = (int)round((double)(ptt_ms * 1000) / sample_duration_us);
  //Logger::log_msg("[RUNNER]", "Total offset:", total_offset);
  pleth_offset = mod(total_offset, waveTable[current_test->wave].no_samples);
  //Logger::log_msg("[RUNNER]", "Pleth offset:", pleth_offset);

  do_ptt_update = false;
}

void Runner::inc_ptt() {
  Runner::set_ptt(ptt_ms + 1);
}

void Runner::dec_ptt() {
  Runner::set_ptt(ptt_ms - 1);
}

void Runner::set_heart_rate(unsigned int bpm) {
  if(!current_test) return;

  heart_rate = bpm;
  do_heart_rate_update = true;
}

void Runner::update_heart_rate() {
  if(!do_heart_rate_update)
    return;
  
  // Calculate the (virtual) sample rate that results in the requested heart rate
  double wave_duration_us = (double)(60 * 1000 * 1000) / heart_rate;
  int sample_duration_us = (int)round((double)wave_duration_us / (double)waveTable[current_test->wave].no_samples);
  Timer4.stop();
  Timer4.setPeriod(sample_duration_us);

  do_heart_rate_update = false;
}

void Runner::inc_heart_rate() {
  Runner::set_heart_rate(heart_rate + 1);
}

void Runner::dec_heart_rate() {
  Runner::set_heart_rate(heart_rate - 1);
}


// Interrupt handler

void sample_interrupt() {
  int no_samples = waveTable[current_test->wave].no_samples;
  int idx_pleth = mod(sample - pleth_offset, no_samples);
  analogWrite(PIN_OUT_PLETH, 4095-waveTable[current_test->wave].pleth_samples[idx_pleth]);
  analogWrite(PIN_OUT_ECG, waveTable[current_test->wave].ecg_samples[sample]);
  sample++;
  if(sample >= no_samples) {
    sample = 0;
    Timer4.stop();
    is_finished = true;
    // Wait for synchronization...
  }
}
