#include "suite.h"
#include "pins.h"
#include "output.h"
#include <DueTimer.h>
#include "logger.h"

#define LONGPRESSTIME_MS  500
#define DEBOUNCE_THRES_MS 20

long lastStartButtonChanged = 0;
bool startButtonRising = false;
bool startButtonHasTimedOut = false;

long lastStopButtonChanged = 0;
bool stopButtonRising = false;
bool stopButtonHasTimedOut = false;

static volatile bool suite_running = false;

void test_start_timeout_isr() {
  Timer0.stop();
  // Long press
  if(suite_running)
    Suite::pause();
  startButtonHasTimedOut = true;
}

void test_start_isr() {
  // Debounce check
  long now = millis();
  if(now - lastStartButtonChanged < DEBOUNCE_THRES_MS)
    return;
  lastStartButtonChanged = now;
  
  if(!startButtonRising) {
    startButtonRising = true;
    Timer0.start();
  } else {
    if(startButtonHasTimedOut) {
      // Long press already executed
      startButtonHasTimedOut = false;
    } else {
      // Short press
      Timer0.stop();
      if(!suite_running)
        suite_running = true;
      else
        Suite::prev();
    }

    startButtonRising = false;
  }
}

void test_stop_timeout_isr() {
  Timer2.stop();
  // Long press
  if(suite_running)
    Suite::stop();
  stopButtonHasTimedOut = true;
}

void test_stop_isr() {
  // Debounce check
  long now = millis();
  if(now - lastStopButtonChanged < DEBOUNCE_THRES_MS)
    return;
  lastStopButtonChanged = now;
  
  if(!stopButtonRising) {
    stopButtonRising = true;
    Timer2.start();
  } else {
    if(stopButtonHasTimedOut) {
      // Long press already executed
      stopButtonHasTimedOut = false;
    } else {
      // Short press
      Timer2.stop();
      if(suite_running)
        Suite::next();
    }

    stopButtonRising = false;
  }
}

void setup() {
  Output::initialize();
  Logger::initialize();
  Suite::initialize();
  attachInterrupt(digitalPinToInterrupt(PIN_IN_START), test_start_isr, CHANGE);
  attachInterrupt(digitalPinToInterrupt(PIN_IN_STOP), test_stop_isr, CHANGE);
  Timer0.attachInterrupt(test_start_timeout_isr).setPeriod((LONGPRESSTIME_MS+10) * 1000);
  Timer2.attachInterrupt(test_stop_timeout_isr).setPeriod((LONGPRESSTIME_MS+10) * 1000);
}

void loop() {
  //suite_running = true;
  if(suite_running) {
    Logger::log_msg("MAIN", "Executing test suite.");
    Suite::start();
    Logger::log_msg("MAIN", "Resumed from test suite.");
    suite_running = false;
  }
  delayMicroseconds(1000);
  Output::draw_display();
}
