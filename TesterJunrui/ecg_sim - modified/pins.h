#ifndef _PINS_H
#define _PINS_H

#define PIN_IN_START          5
#define PIN_IN_STOP           7
#define PIN_OUT_ECG           DAC0
#define PIN_OUT_PLETH         DAC1

#endif
