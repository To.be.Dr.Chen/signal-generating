#ifndef _TEST_FACTORY_H
#define _TEST_FACTORY_H

class Test;

namespace TestFactory {

  Test* get_test(int no);
  
}

#endif
