#ifndef _TEST_H
#define _TEST_H

// Number of test classes
#define MAX_TEST_NO 11

#include "runner.h"
#include "output.h"
#include <DueTimer.h>

class Test {
  public:
    Test(int duration_sec, int bpm_start, int bpm_stop, int ptt_start, int ptt_stop, int wave) : duration_sec(duration_sec), wave(wave), bpm_start(bpm_start), bpm_stop(bpm_stop), ptt_start(ptt_start), ptt_stop(ptt_stop){};
    
    virtual void initialize() {
      Runner::set_heart_rate(bpm_start);
      Runner::set_ptt(ptt_start);

      if(bpm_start != bpm_stop)
        initialize_hr_timer();      
    
      if(ptt_start != ptt_stop)
        initialize_ptt_timer();
    }
    
    virtual void initialize_hr_timer() {
      Timer6.stop();
      int freq_us = (int)round((double)(60 * 1000 * 1000) / abs(bpm_stop-bpm_start));
      if(bpm_start < bpm_stop)
        Timer6.attachInterrupt(Runner::inc_heart_rate).setPeriod(freq_us);
      else
        Timer6.attachInterrupt(Runner::dec_heart_rate).setPeriod(freq_us);        
      Timer6.start();
    }

    virtual void initialize_ptt_timer() {
      Timer1.stop();
      int freq_us = (int)round((double)(60 * 1000 * 1000) / abs(ptt_stop-ptt_start));
      if(ptt_start < ptt_stop)
        Timer1.attachInterrupt(Runner::inc_ptt).setPeriod(freq_us);
      else
        Timer1.attachInterrupt(Runner::dec_ptt).setPeriod(freq_us);        
      Timer1.start();        
    }

    virtual void finalize() {
      if(bpm_start != bpm_stop)
        Timer6.stop();
      if(ptt_start != ptt_stop)
        Timer1.stop();
    }

    virtual void display(int no, bool is_running) {
      Output::display_test(no, duration_sec, bpm_start, bpm_stop, ptt_start, ptt_stop, is_running);
    }
    
    const int duration_sec;
    const int wave;
    const int bpm_start;
    const int bpm_stop;
    const int ptt_start;
    const int ptt_stop;
};

class Test1 : public Test {
  public:
    Test1();
};

class Test2 : public Test {
  public:
    Test2();
};

class Test3 : public Test {
  public:
    Test3();
};

class Test4 : public Test {
  public:
    Test4();
};

class Test5 : public Test {
  public:
    Test5();
};

class Test6 : public Test {
  public:
    Test6();
};

class Test7 : public Test {
  public:
    Test7();
};

class Test8 : public Test {
  public:
    Test8();
};

class Test9 : public Test {
  public:
    Test9();
    virtual void initialize();
    virtual void finalize();
};

class Test10 : public Test {
  public:
    Test10();
};

class Test11 : public Test {
  public:
    Test11();
};

#endif
