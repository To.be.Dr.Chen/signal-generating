#include "suite.h"
#include "test_factory.h"
#include "test.h"
#include "runner.h"
#include "output.h"
#include "logger.h"

volatile static int current_test_no = 1;
static Test* current_test = 0;
volatile static bool suite_running = false;

using namespace Runner;

void Suite::initialize() {
  Runner::initialize();
  Test* test = TestFactory::get_test(current_test_no);
  test->display(current_test_no, suite_running);
  delete test;
}

void Suite::start() {
  Logger::log_msg("SUITE", "Start suite.");
  if(suite_running) return;
  AbortReason res = NO_ABORT;
  
  while (current_test_no <= MAX_TEST_NO) {
    current_test = TestFactory::get_test(current_test_no);
    suite_running = true;
    current_test->display(current_test_no, suite_running);
    Logger::log_msg("SUITE", "Start test no", current_test_no);
    res = Runner::run(current_test);
    Logger::log_msg("SUITE", "Finished test no", current_test_no);
    if(res == FINISH || res == NEXT) {
      // Test finished nominally or next test has been requested, proceed to next test
      current_test_no++;
    } else if(res == PREV) {
      // If previous test has been requested, do previous test
      current_test_no--;
      if(current_test_no <= 0) current_test_no = 1;
    }
    if(current_test) {
      delete current_test;
      current_test = 0;
    }
    if(res == PAUSE) {
      break;
    } else if(res == ABORT) {
      current_test_no = 1;
      break;
    } else if(res == FAILED) {
      Logger::log_msg("SUITE", "Failed test no", current_test_no);
      current_test_no = 1;
      break;
    }
  }
  if(current_test_no > MAX_TEST_NO)
    current_test_no = 1;
  suite_running = false;
  Test* test = TestFactory::get_test(current_test_no);
  test->display(current_test_no, suite_running);
  delete test;
}

void Suite::stop() {
  if(suite_running)
    Runner::abort();
}

void Suite::pause() {
  if(suite_running)
    Runner::abort(PAUSE);
}

void Suite::next() {
  if(suite_running) {
    Runner::abort(NEXT);
  }
}

void Suite::prev() {
  if(suite_running) {
    Runner::abort(PREV);
  }
}

