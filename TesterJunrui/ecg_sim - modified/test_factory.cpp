#include "test_factory.h"
#include "test.h"

Test* TestFactory::get_test(int no) {
  if(no > MAX_TEST_NO) return 0;
  
  switch(no) {
    case 1: { return new Test1(); break; }
    case 2: { return new Test2(); break; }
    case 3: { return new Test3(); break; }
    case 4: { return new Test4(); break; }
    case 5: { return new Test5(); break; }
    case 6: { return new Test6(); break; }
    case 7: { return new Test7(); break; }
    case 8: { return new Test8(); break; }
    case 9: { return new Test9(); break; }
    case 10: { return new Test10(); break; }
    case 11: { return new Test11(); break; }
  }
}

