#ifndef _Waveforms_h_
#define _Waveforms_h_

#include "waves.h"

#define ASSEMBLE_WAVES(ecg, pleth, sr) {(int*)&ecg, (int*)&pleth, sizeof(ecg)/sizeof(int), sr}

typedef struct _wave_t {
  int* ecg_samples;
  int* pleth_samples;
  int no_samples;
  int sample_rate;
} wave_t;

// Make sure to only use templates of equal size and sample rate together!
static wave_t waveTable[] = {
   ASSEMBLE_WAVES(ecg_1, pleth_1, 1000)
};

// Sizes of the tables
static int no_waves = sizeof(waveTable)/sizeof(wave_t);

#endif 
