#ifndef _LOGGER_H
#define _LOGGER_H

namespace Logger{
  void initialize();
  void log_msg(const char* c, const char* s);
  void log_msg(const char* c, const char* s, int i);
  void log_msg(const char* c, const char* s, double d);
}

#endif
